### ADNCode - Posts image of selection w/comment to App.net (ADN) stream.

[https://github.com/mgrimes/adncode](https://github.com/mgrimes/adncode)

This is a JetBrains IDE plugin and **requires** an App.net (ADN) account.

##### Features

+ Retina-ready PNG image of text selection.
+ Least privilege scope ("write_post") for web service communication.

##### Authentication and Authorization

Your API access token will be stored in `ADNSettings.xml` once authenticated. Without a token, your browser will be launched to provide a token that can be cut/paste into the dialog prompting for "Token". To invoke the browser to obtain an API access token, click OK on the Token input dialog without supplying a token. (Careful not to reload the browser after you have copied the token as each browser reload will provide a new access token).

Once you are authorized, you will no longer be prompted for a Token.

##### Posting

Once authorized, you will be able to supply a post to accompany the image of your text selection. Posts are required; you must say something about the text selection being posted as an image annotation to an ADN post. This will prevent images from being posted that rely on the text selection providing code context. Take a moment to declare your intent as to why you are posting the text selection to your App.net stream for the benefit of your followers.

Posts can be 250 characters in length. The last six are reserved for " Image" hyperlink to raw image URI.

##### Using ADNCode with multiple JetBrains IDEs

Once one installation of the plugin has been authorized with ADN, any future installs of ADNCode on alternate IDEs will generate a new access token unless the settings file is shared between IDEs. This will prevent previous installations from working.

The easiest workaround is to copy `ADNSettings.xml` to each of your JetBrain's plugin configuration folders for continuity.

For example, on OSX, IntelliJ IDEA 13 stores `ADNSettings.xml` in  `~/Library/Preferences/IntelliJIdea13/options/`. Whereas AppCode 3 stores `ADNSettings.xml` in `~/Library/Preferences/appCode30/options`. Once authorized in one IDE, copy `ADNSettings.xml` into the other IDE's plugin configuration folder.
