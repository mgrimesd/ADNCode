package net.stateful.jetbrains.adncode;

import com.intellij.codeInsight.hint.EditorFragmentComponent;
import com.intellij.ide.ui.UISettings;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.InputValidator;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.ui.awt.RelativePoint;
import com.intellij.util.JBHiDPIScaledImage;
import com.intellij.util.ui.UIUtil;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Jetbrains IDE entry point for ADNCode.
 */
public class ADNCodeAction extends AnAction {

    private static final int POPUP_FADEOUT_TIME = 7500;

    private static final Logger LOG = Logger.getInstance(ADNCodeAction.class);

    public ADNCodeAction() {
        super(null, null, IconLoader.findIcon("/adn-16x16.png"));
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        final Project project = event.getRequiredData(LangDataKeys.PROJECT);
        final Editor editor = event.getRequiredData(LangDataKeys.EDITOR);
        final Document document = editor.getDocument();
        final SelectionModel selectionModel = editor.getSelectionModel();

        final int start = selectionModel.getSelectionStart();
        final int end = selectionModel.getSelectionEnd();

        final int startLine = document.getLineNumber(start);
        final int endLine = document.getLineNumber(end) + 1;
        selectionModel.removeSelection();

        final EditorFragmentComponent fragment = EditorFragmentComponent.
                createEditorFragmentComponent(editor, startLine, endLine, false, false);

        final byte[] image = createImage(fragment);
        if (isImageCreated(image))
            postToADN(project, event, image);

        selectionModel.setSelection(start, end);
    }

    private boolean isImageCreated(byte[] image) {
        return null != image;
    }

    private void postToADN(Project project, AnActionEvent event, byte[] image) {
        String filename = createFilename();

        authorize(project);

        final String postText = requestCodeCommentFromUser(project);
        if (isEmptyPost(postText)) return;

        try {
            if (ADNClient.uploadFile(filename, image) == 401) {
                displayInvalidTokenMessage(event);
                requestAuthorization(project);
            }
        } catch (OAuthSystemException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (OAuthProblemException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (ADNClient.sendPost(postText) == 200)
                displayPostCompletedDialog(event);
        } catch (OAuthSystemException e) {
            e.printStackTrace();
        } catch (OAuthProblemException e) {
            e.printStackTrace();
        }

    }

    private String requestCodeCommentFromUser(Project project) {
        return Messages.
                    showMultilineInputDialog(project, "Code Comment", "Comment", "", null, new InputValidator() {
                        @Override
                        public boolean checkInput(String s) {
                            return canClose(s);
                        }

                        @Override
                        public boolean canClose(String s) {
                            // Reserve 6 chars for " Image" link entity wiring.
                            return s.length() > 0 && s.length() <= 250;
                        }
                    });
    }

    private String createFilename() {
        String timestamp = new SimpleDateFormat("yyyy'-'MM'-'dd'-'hh'.'mm'.'ss").format(new Date());
        return "ADNCode-" + timestamp + ".png";
    }

    private void authorize(Project project) {
        if (!isTokenPersisted()) {
            try {
                requestAuthorization(project);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isTokenPersisted() {
        return ADNSettings.getInstance().token != null;
    }

    private void requestAuthorization(Project project) throws Exception {
        final String token = requestTokenFromUser(project);
        if (token != null) {
            if (StringUtil.isEmpty(token)) {
                String uri = ADNClient.authorize();
                Desktop.getDesktop().browse(new URI(uri));
            } else {
                ADNSettings.getInstance().token = token;
            }
        }
    }

    private String requestTokenFromUser(Project project) {
        return Messages.showInputDialog(project, "Token (Leave empty and click OK to retrieve token)", "ADN Authorization", null);
    }

    private void displayInvalidTokenMessage(AnActionEvent event) {
            StatusBar statusBar = WindowManager.getInstance()
                    .getStatusBar(PlatformDataKeys.PROJECT.getData(event.getDataContext()));

            assert statusBar != null;
            JBPopupFactory.getInstance()
                    .createHtmlTextBalloonBuilder("Unauthorized access to App.net - Presumably bad token value", MessageType.ERROR, null)
                    .setFadeoutTime(POPUP_FADEOUT_TIME)
                    .createBalloon()
                    .show(RelativePoint.getCenterOf(statusBar.getComponent()), Balloon.Position.atRight);
    }

    private boolean isEmptyPost(String postText) {
        return postText == null || postText.equals("");
    }

    private void displayPostCompletedDialog(AnActionEvent event) {
        StatusBar statusBar = WindowManager.getInstance()
                .getStatusBar(PlatformDataKeys.PROJECT.getData(event.getDataContext()));

        assert statusBar != null;
        JBPopupFactory.getInstance()
                .createHtmlTextBalloonBuilder("Successfully posted to ADN.", MessageType.INFO, null)
                .setFadeoutTime(POPUP_FADEOUT_TIME)
                .createBalloon()
                .show(RelativePoint.getCenterOf(statusBar.getComponent()), Balloon.Position.atRight);
    }

    private byte[] createImage(EditorFragmentComponent fragment) {
        final BufferedImage image = createBufferedImage(fragment);
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "png", stream);
        } catch (Exception exception) {
            LOG.error(exception);
            return null;
        }

        return stream.toByteArray();
    }

    private BufferedImage createBufferedImage(EditorFragmentComponent fragment) {
        final Dimension size = fragment.getPreferredSize();
        fragment.setSize(size);
        fragment.doLayout();

        final BufferedImage image;
        // Broken on JRE 1.6 (default for OSX Jetbrains IDEs)
//        if (UIUtil.isAppleRetina())
//            image = AppleHiDPIScaledImage.create(size.width, size.height, BufferedImage.TYPE_INT_RGB);
        if (UIUtil.isRetina())
            image = new JBHiDPIScaledImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
        else
            image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);

        final Graphics graphics = image.getGraphics();
        UISettings.setupAntialiasing(graphics);
        fragment.printAll(graphics);
        graphics.dispose();

        return image;
    }

    @Override
    public void update(AnActionEvent e) {
        final Project project = e.getData(LangDataKeys.PROJECT);
        final Editor editor = e.getData(LangDataKeys.EDITOR);

        e.getPresentation().setVisible(project != null && editor != null && editor.getSelectionModel().hasSelection());
    }
}
