package net.stateful.jetbrains.adncode;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.intellij.openapi.diagnostic.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

public class ADNClient {
    private static final String ADN_AUTHN_ENDPOINT = "https://account.app.net/oauth/authenticate";
    private static final String ADN_FILES_ENDPOINT = "https://api.app.net/files";
    private static final String ADN_POSTS_ENDPOINT = "https://api.app.net/posts";
    private static final String ADNCODE_CLIENT_ID = "pLfhraBQBFSKWEjTy8bZUJgVVN9UPt59";
    private static final String CRLF = "\r\n";

    private static String boundary;
    private static String fileId;
    private static String fileToken;
    private static String imageUrl;

    private static final Logger LOG = Logger.getInstance(ADNClient.class);

    /**
     * Authorizes client with App.net
     *
     * @return locationUri String
     *
     * @throws IOException
     * @throws OAuthSystemException
     */
    public static String authorize() throws Exception {
        OAuthClientRequest authzRequest = OAuthClientRequest
                .authorizationLocation(ADN_AUTHN_ENDPOINT)
                .setClientId(ADNCODE_CLIENT_ID)
                .setScope("write_post")
                .setResponseType("token")
                .setRedirectURI("urn:ietf:wg:oauth:2.0:oob")
                .setState(generateCsrfToken())
                .buildQueryMessage();
        authzRequest.setHeader(OAuth.HeaderType.CONTENT_TYPE, "application/x-www-form-urlencoded");
        authzRequest.setHeader("Referer", "https://account.app.net");

        return authzRequest.getLocationUri();
    }

    /**
     * Uploads screenshot to ADN File API
     *
     * @param filename name of file
     * @param picture screenshot data
     *
     * @return responseCode int
     *
     * @throws OAuthSystemException
     * @throws UnsupportedEncodingException
     * @throws OAuthProblemException
     */
    public static int uploadFile(String filename, byte[] picture) throws OAuthSystemException, UnsupportedEncodingException, OAuthProblemException {
        OAuthClientRequest request = new OAuthBearerClientRequest(ADN_FILES_ENDPOINT)
                .buildBodyMessage();

        request.setHeader(OAuth.HeaderType.AUTHORIZATION, "Bearer " + ADNSettings.getInstance().token);
        setBoundary("-----" + System.currentTimeMillis());
        request.setHeader(OAuth.HeaderType.CONTENT_TYPE, "multipart/form-data; boundary=" + getBoundary());
        request.setHeader("X-ADN-Pretty-JSON", "1");
        request.setHeader("Accept", "*/*");

        String body =
                addFormField("kind", "image")
                + addFormField("mime_type", "image/png")
                + addFormField("name", filename)
                + addFormField("public", "1")
                + addFormField("type", "net.stateful.jetbrains.adncode.image")
                + addContentFormField(filename, Base64.encodeBase64String(picture));

        request.setHeader("Content-Length", String.valueOf(body.length()));
        request.setBody(body);

        OAuthClient client = new OAuthClient(new URLConnectionClient());
        OAuthResourceResponse response = client.resource(request, OAuth.HttpMethod.POST, OAuthResourceResponse.class);
        if (response.getResponseCode() == 200)
            preserveFileMetadata(response);

        return response.getResponseCode();
    }

    /**
     *
     * @param postText
     * @return
     * @throws OAuthSystemException
     * @throws OAuthProblemException
     */
    public static int sendPost(String postText) throws OAuthSystemException, OAuthProblemException {
        OAuthClientRequest request = new OAuthBearerClientRequest(ADN_POSTS_ENDPOINT)
                .buildBodyMessage();
        request.setHeader(OAuth.HeaderType.AUTHORIZATION, "Bearer " + ADNSettings.getInstance().token);
        request.setHeader(OAuth.HeaderType.CONTENT_TYPE, "application/json");
        request.setHeader("X-ADN-Pretty-JSON", "1");

        request.setBody(buildPost(postText, getFileId(), getFileToken()));

        OAuthClient client = new OAuthClient(new URLConnectionClient());

        OAuthResourceResponse resourceResponse = client.resource(request, OAuth.HttpMethod.POST, OAuthResourceResponse.class);
        return resourceResponse.getResponseCode();
    }

    private static String generateCsrfToken() {
        // TODO: Need to verify token (promote to field)
        return UUID.randomUUID().toString();
    }

    private static void preserveFileMetadata(OAuthResourceResponse multipartResponse) {
        assert multipartResponse.getResponseCode() == 200;
        setFileId(decodeFileId(multipartResponse));
        setFileToken(decodeFileToken(multipartResponse));
        setImageUrl(decodeShortUrl(multipartResponse));
    }

    private static String addFormField(String name, String value) {
        return "--" + getBoundary() + CRLF
                + "Content-Disposition: form-data; name=\"" + name + "\"" + CRLF + CRLF
                + value + CRLF;
    }

    private static String addContentFormField(String filename, String fileData) {
        return "--" + getBoundary() + CRLF
                + "Content-Disposition: file; name=\"content\"; filename=\"" + filename + "\"" + CRLF
                + "Content-Transfer-Encoding: base64" + CRLF
                + "Content-Type: image/png" + CRLF + CRLF
                + fileData + CRLF
                + "--" + getBoundary() + "--" + CRLF;
    }

    private static String decodeFileId(OAuthResourceResponse response) {
        JsonObject json = JsonObject.readFrom(response.getBody());
        return json.get("data").asObject().get("id").asString();
    }

    private static String decodeFileToken(OAuthResourceResponse response) {
        JsonObject json = JsonObject.readFrom(response.getBody());
        return json.get("data").asObject().get("file_token").asString();
    }

    private static String decodeShortUrl(OAuthResourceResponse response) {
        JsonObject json = JsonObject.readFrom(response.getBody());
        return json.get("data").asObject().get("url_short").asString();
    }

    private static String buildPost(String postText, String fileId, String fileToken) {
        JsonObject fileAnnotation = new JsonObject()
                .add("file_id", fileId)
                .add("file_token", fileToken)
                .add("format", "oembed");

        JsonObject core = new JsonObject().add("+net.app.core.file", fileAnnotation);
        JsonObject oembedType = new JsonObject()
                .add("type", "net.app.core.oembed")
                .add("value", core);

        JsonArray annotations = new JsonArray().add(oembedType);

        JsonObject link = new JsonObject()
                .add("len", 5)
                .add("pos", postText.length() + 1)
                .add("text", "Image")
                .add("url", getImageUrl());

        JsonArray links = new JsonArray().add(link);
        JsonObject entities = new JsonObject().add("links", links);

        JsonObject adnPost = new JsonObject()
                .add("text", postText + " Image")
                .add("entities", entities)
                .add("annotations", annotations);

        return adnPost.toString();
    }

    public static String getBoundary() {
        return boundary;
    }

    public static void setBoundary(String boundary) {
        ADNClient.boundary = boundary;
    }

    public static String getFileId() {
        return fileId;
    }

    public static void setFileId(String fileId) {
        ADNClient.fileId = fileId;
    }

    public static String getFileToken() {
        return fileToken;
    }

    public static void setFileToken(String fileToken) {
        ADNClient.fileToken = fileToken;
    }

    public static String getImageUrl() {
        return imageUrl;
    }

    public static void setImageUrl(String imageUrl) {
        ADNClient.imageUrl = imageUrl;
    }

}
