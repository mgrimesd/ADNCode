package net.stateful.jetbrains.adncode;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.components.StoragePathMacros;
import org.jetbrains.annotations.Nullable;

@State(
        name = "ADNCodeSettings",
        storages = {
                @Storage(file = StoragePathMacros.APP_CONFIG + "/ADNCode_settings.xml")
        }
)

public class ADNSettings implements PersistentStateComponent<ADNSettings> {
    public String token;

    @Nullable
    @Override
    public ADNSettings getState() {
        return this;
    }

    @Override
    public void loadState(ADNSettings adnSettings) {
        token = adnSettings.token;
    }

    public static ADNSettings getInstance() {
        return ServiceManager.getService(ADNSettings.class);
    }
}
